const express = require('express');
const app = express();

app.get('/', (req,res)=>{
    res.send({name:'Rohan Arihant'});
});

const PORT = process.env.PORT || 5000;
app.listen(PORT, function(){
    console.log(`App is listening on port:- ${PORT}`);
})